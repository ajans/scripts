--USE hkm_dwh

declare @startdate date = '2017-03-01'
DECLARE @endDate date = '2018-02-28'

DECLARE @tt1 TABLE 
(
 countryID varchar(5)
 , locationID varchar(255)
 , jobTitle varchar(255)
 , openingCount float
)

INSERT INTO @tt1
SELECT 
 ISNULL(stores.countryID,
    CASE WHEN GROUPING(stores.countryID) = 0 THEN 'UNKNOWN' ELSE 'ALL' END)
,  ISNULL(vthhd.locationID,
    CASE WHEN GROUPING(vthhd.locationID) = 0 THEN 'UNKNOWN' ELSE 'ALL' END)
,   ISNULL(vthhd.JobTitle,
    CASE WHEN GROUPING(vthhd.JobTitle) = 0 THEN 'UNKNOWN' ELSE 'ALL' END)
, openingCount=count(*)
FROM dbo.view_Tableau_HR_Headcount_Data  vthhd
LEFT OUTER JOIN dbo.ds_XL_ECCMasterDataInput_Tableau_Stores stores
ON convert(varchar(10), stores.storeID) = vthhd.LocationID
AND stores.dss_current_flag  = 'Y' 
WHERE calendarDate = @startdate 
AND vthhd.TerminationDate  IS NULL
AND vthhd.jobTitle IN  ('Fashionista', 'Lingersta', 'Lingerie Expert', 'Lingerie Ambassador')
GROUP BY ROLLUP ( stores.countryID, vthhd.locationID, vthhd.jobTitle )
ORDER BY stores.countryID, vthhd.locationID, vthhd.JobTitle


DECLARE @tt2 table
(
 countryID varchar(5)
 , locationID varchar(255)
 , jobTitle varchar(255)
 , outflowCount float
)


INSERT INTO @tt2
SELECT 
  ISNULL(st.countryID,
  CASE WHEN GROUPING(st.countryID) = 0 THEN 'UNKNOWN' ELSE 'ALL' END)
,  ISNULL(vthhd.locationID,
    CASE WHEN GROUPING(vthhd.locationID) = 0 THEN 'UNKNOWN' ELSE 'ALL' END)
,   ISNULL(vthhd.JobTitle,
    CASE WHEN GROUPING(vthhd.JobTitle) = 0 THEN 'UNKNOWN' ELSE 'ALL' END)
, outFlow=count(DISTINCT vthhd.employeeID)
FROM dbo.view_Tableau_HR_Headcount_Data vthhd
LEFT OUTER JOIN dbo.ds_XL_ECCMasterDataInput_Tableau_Stores st
ON convert(varchar(10), st.storeID) = vthhd.LocationID
AND st.dss_current_flag  = 'Y' 
WHERE vthhd.calendarDate BETWEEN @startdate AND @endDate
AND vthhd.TerminationDate  IS NOT NULL
AND vthhd.JobTitle	IN  ('Fashionista', 'Lingersta', 'Lingerie Expert', 'Lingerie Ambassador')
GROUP BY ROLLUP	(st.countryID, vthhd.locationId, vthhd.JobTitle)

SELECT * FROM @tt2 t WHERE t.countryid = 'AT' ''

SELECT t1.*, t2.outflowCount, t2.outflowCount / t1.openingCount
FROM @tt1 t1
LEFT OUTER JOIN @tt2 t2 
ON  
t2.countryID = t1.countryID
AND t2.locationID = t1.locationID
AND t2.jobTitle = t1.jobTitle
WHERE coalesce(t1.countryID, t2.countryID) = 'AT'
ORDER BY t1.countryID, t1.locationID, t1.jobTitle

