select x.[object_id], x.[index_type_desc], x.[avg_fragmentation_in_percent], o.name
from
(
SELECT * FROM sys.dm_db_index_physical_stats (5, NULL, NULL, NULL, NULL)
) x
inner join sys.objects o
on o.object_id = x.object_id
order by 3 desc

