select employeeId
, len(employeeId)
, datalength(employeeID)
, replace(employeeID, char(160), '') 
from ds_workday_HR_Workers_Active_in_Period 
where replace(employeeID, char(160), '') = '3508120'

DECLARE @counter int = 1;
DECLARE @colString varchar(10) = '3508120 ';

WHILE @counter <= DATALENGTH(@colString)
   BEGIN
   SELECT CHAR(ASCII(SUBSTRING(@colString, @counter, 1))) as [Character],
   ASCII(SUBSTRING(@colString, @counter, 1)) as [ASCIIValue]
     
   SET @counter = @counter + 1
   END
GO

select distinct employeeID, len(employeeID) from stage_010_HR_Workers_Active_in_Period

