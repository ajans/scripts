DECLARE @v_dss_create_time datetime = getdate()
DECLARE @v_dss_update_time datetime = getdate()

SELECT DISTINCT cpsdc.[0fiscper], cpsdc.[0calDay], cpsdc.[0plant] storeID
, SUM(cpsdc.XHR_GRLC) OVER ( PARTITION BY cpsdc.[0PLANT] ORDER BY cpsdc.[0calday]) AS RunningTotal
, py.RunningTotal runningTotalPY
FROM hana.dbo.CV012_POS_SALESPLAN_Daily_CFY cpsdc
LEFT OUTER JOIN 
(
SELECT distinct cpsdp.[0fiscper], cpsdp.[0calDay], cpsdp.[0plant] storeID
, SUM(cpsdp.XHR_GRLC) OVER ( PARTITION BY cpsdp.[0PLANT] ORDER BY cpsdp.[0calday]) AS RunningTotal
FROM hana.dbo.CV012_POS_SALESPLAN_Daily_PFY cpsdp
WHERE cpsdp.[0fiscper] = 2016051
) py ON py.[0fiscper] + 1000 = cpsdc.[0FISCPER]
AND datepart(dw, convert(datetime, py.[0calDay])) = datepart(dw, convert(datetime, cpsdc.[0CALDAY]))
AND py.storeId = cpsdc.[0plant]
WHERE cpsdc.[0FISCPER] = 2017051
AND cpsdc.[0plant] = 2485
ORDER BY 3, 2



-- 2.021 	 967 	 2.065 	 -   	 -   	 -   	 -   	 5.053 
-- 2.560 	 2.062 	 2.081 	 2.282 	 2.998 	 2.568 	 5.043 	 19.596 

 SELECT
    stage_020_Tableau_Prep_Daily_Merge.calendarDate,
    stage_020_Tableau_Prep_Daily_Merge.fiscalYearPeriod,
    stage_020_Tableau_Prep_Daily_Merge.storeID,
    stage_010_ECC_Master_Store_inc_Change_full.storeIDName,
    stage_010_ECC_Master_Store_inc_Change_full.storeCategoryMove,
    stage_010_ECC_Master_Store_inc_Change_full.storeConceptID,
    stage_010_ECC_Master_Store_inc_Change_full.storeConceptName,
    stage_010_ECC_Master_Store_inc_Change_full.channelID,
    stage_010_ECC_Master_Store_inc_Change_full.channelName,
    stage_010_ECC_Master_Store_inc_Change_full.storeMaturityID,
    stage_010_ECC_Master_Store_inc_Change_full.storeMaturityName,
    stage_010_ECC_Master_Store_inc_Change_full.countryID,
    stage_010_ECC_Master_Store_inc_Change_full.countryname,
    stage_010_ECC_Master_Store_inc_Change_full.regionID,
    stage_010_ECC_Master_Store_inc_Change_full.regionName,
    stage_010_ECC_Master_Store_inc_Change_full.regionManager,
    stage_010_ECC_Master_Store_inc_Change_full.storeManagerName,
    stage_010_ECC_Master_Store_inc_Change_full.storeLatitude,
    stage_010_ECC_Master_Store_inc_Change_full.storeLongitude,
    stage_010_ECC_Master_Store_inc_Change_full.regionLatitude,
    stage_010_ECC_Master_Store_inc_Change_full.regionLongitude,
    stage_010_ECC_Master_Store_inc_Change_full.trainerName,
    stage_020_Tableau_Prep_Daily_Merge.grossSales
-
stage_020_Tableau_Prep_Daily_Merge.markdownTotal InitialSales,
    stage_020_Tableau_Prep_Daily_Merge.netSales
*
stage_010_ECC_Master_Store_inc_Change_full.percentageVATAfter2015 VAT,
    stage_020_Tableau_Prep_Daily_Merge.grossSales
-
stage_020_Tableau_Prep_Daily_Merge.markdownTotal
-
(
 stage_020_Tableau_Prep_Daily_Merge.netSales
 *
 stage_010_ECC_Master_Store_inc_Change_full.percentageVATAfter2015
)
-
stage_020_Tableau_Prep_Daily_Merge.costOfSales InitialSalesVATCostOfSales,
    stage_020_Tableau_Prep_Daily_Merge.grossSales,
    
    SUM(stage_020_Tableau_Prep_Daily_Merge.grossSales) 
	   OVER ( PARTITION BY stage_020_Tableau_Prep_Daily_Merge.storeID, stage_020_Tableau_Prep_Daily_Merge.fiscalYearPeriod
			ORDER BY stage_020_Tableau_Prep_Daily_Merge.calendarDate) AS RunningTotal,
    
    stage_020_Tableau_Prep_Daily_Merge.grossSalesExclPromotions,
    stage_020_Tableau_Prep_Daily_Merge.grossSalesBdgSource,
    stage_020_Tableau_Prep_Daily_Merge.costOfSales,
    stage_020_Tableau_Prep_Daily_Merge.salesPOSReceiptMeansOfPayment,
    stage_020_Tableau_Prep_Daily_Merge.piecesSoldInclPromotions,
    stage_020_Tableau_Prep_Daily_Merge.piecesSoldExclPromotions,
    stage_020_Tableau_Prep_Daily_Merge.markdownSalesZB10,
    stage_020_Tableau_Prep_Daily_Merge.markdownPromotionsZB12,
    stage_020_Tableau_Prep_Daily_Merge.markdownSalesDiscountZB14,
    stage_020_Tableau_Prep_Daily_Merge.markdownMarketingZB11ZB16inclPriceDifference,
    stage_020_Tableau_Prep_Daily_Merge.netSales,
    stage_020_Tableau_Prep_Daily_Merge.markdownTotal,
    stage_010_CV008_Conversion_Daily.visitors,
    stage_010_CV008_Conversion_Daily.visitorsCorrected,
    stage_010_CV008_Conversion_Daily.nrOfReceiptsCustomers,
    stage_010_CV008_Conversion_Daily.nrOfReceiptsCustomersCorrected,
    stage_010_CV008_Conversion_Daily.piecesSold,
    ds_XL_GrossMarginBudget_Weekly_Stores.grossSalesBudget,
    ds_XL_GrossMarginBudget_Weekly_Stores.netSalesBudget,
    @v_dss_create_time,
    @v_dss_update_time
  FROM dbo.stage_020_Tableau_Prep_Daily_Merge stage_020_Tableau_Prep_Daily_Merge
  
  INNER JOIN dbo.stage_010_FiscalCalendar_Daily_Incl_Current stage_010_FiscalCalendar_Daily_Incl_Current
  ON  stage_020_Tableau_Prep_Daily_Merge.FiscalYearPeriod    = stage_010_FiscalCalendar_Daily_Incl_Current.[Fiscal Year/Period] 
  AND stage_020_Tableau_Prep_Daily_Merge.CalendarDate	    = stage_010_FiscalCalendar_Daily_Incl_Current.[date]
  
  LEFT OUTER JOIN dbo.stage_010_CV008_Conversion_Daily stage_010_CV008_Conversion_Daily
  ON stage_010_CV008_Conversion_Daily.storeID		 = stage_020_Tableau_Prep_Daily_Merge.storeID
  AND stage_010_CV008_Conversion_Daily.fiscalYearPeriod = stage_020_Tableau_Prep_Daily_Merge.fiscalYearPeriod
  AND stage_010_CV008_Conversion_Daily.calendarDay	 = stage_020_Tableau_Prep_Daily_Merge.calendarDate
  
  LEFT OUTER JOIN [dbo].[stage_010_ECC_Master_Store_inc_Change_full] stage_010_ECC_Master_Store_inc_Change_full
  ON stage_020_Tableau_Prep_Daily_Merge.StoreID = stage_010_ECC_Master_Store_inc_Change_full.StoreID
  AND stage_010_ECC_Master_Store_inc_Change_full.rowNumber = 1
  
  LEFT OUTER JOIN (
  			select FiscalYearWeek 
  			, countryID
  			, storeID
  			, grossMarginBudget
                      , grossSalesBudget
  			, netSalesBudget 
  		       FROM [dbo].[ds_XL_GrossMarginBudget_Weekly_Stores]
  		  ) ds_XL_GrossMarginBudget_Weekly_Stores
  on ds_XL_GrossMarginBudget_Weekly_Stores.[fiscalYearWeek] = stage_020_Tableau_Prep_Daily_Merge.FiscalYearPeriod 
  AND ds_XL_GrossMarginBudget_Weekly_Stores.[CountryID] = stage_010_ECC_Master_Store_inc_Change_full.[CountryID]
  AND ds_XL_GrossMarginBudget_Weekly_Stores.[StoreID] = stage_020_Tableau_Prep_Daily_Merge.StoreID

  WHERE stage_020_Tableau_Prep_Daily_Merge.storeID   IN (2485, 2484)