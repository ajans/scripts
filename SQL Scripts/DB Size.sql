SELECT 
    DB_NAME(mf.database_id) AS 'DB Name', 
    name AS 'File Logical Name',
    'File Type' = CASE WHEN type_desc = 'LOG' THEN 'Log File' WHEN type_desc = 'ROWS' THEN 'Data File' ELSE type_desc END,
    mf.physical_name AS 'File Physical Name', 
    size_on_disk_bytes/ 1024 AS 'Size(KB)', 
    size_on_disk_bytes/ 1024 / 1024 AS 'Size(MB)',
    size_on_disk_bytes/ 1024 / 1024 / 1024 AS 'Size(GB)'
FROM 
    sys.dm_io_virtual_file_stats(NULL, NULL) AS divfs 
    JOIN sys.master_files AS mf 
        ON mf.database_id = divfs.database_id 
            AND mf.file_id = divfs.file_id
ORDER BY 
    DB_NAME(mf.database_id)



-- Individual File Sizes and space available for current database
-- (Query 36) (File Sizes and Space)
SELECT f.name AS [File Name] , f.physical_name AS [Physical Name], 
CAST((f.size/128.0) AS DECIMAL(15,2)) AS [Total Size in MB],
CAST((f.size/128.0) AS DECIMAL(15,2)) / 1024 AS [Total Size in GB],
CAST(f.size/128.0 - CAST(FILEPROPERTY(f.name, 'SpaceUsed') AS int)/128.0 AS DECIMAL(15,2))
AS [Available Space In MB], 
CAST(f.size/128.0 - CAST(FILEPROPERTY(f.name, 'SpaceUsed') AS int)/128.0 AS DECIMAL(15,2)) / 1024.00
AS [Available Space In GB], 
[file_id], fg.name AS [Filegroup Name]
FROM sys.database_files AS f WITH (NOLOCK) 
LEFT OUTER JOIN sys.data_spaces AS fg WITH (NOLOCK) 
ON f.data_space_id = fg.data_space_id OPTION (RECOMPILE);