select *
from
(
  select 
    plant AS StoreID,
    type as T,
    col AS ProductGroup, 
    value
  from dbo.AJ_CrossSellPFY_v2 acsc 
  
  cross apply 
  (
    values ('Bras', Bras),
		  ('BrasBriefs', BrasBriefs),
		  ('Bottoms', Bottoms),
		  ('Tops', Tops),
		  ('Lingerie', Lingerie),
		  ('Shapewear', Shapewear),
		  ('Nightshirts', Nightshirts),
		  ('Pyjamas', Pyjamas),
		  ('Robes', Robes),
		  ('BathingSuits', BathingSuits),
		  ('TopsBottomsSwim', TopsBottomsSwim),
		  ('Coordinates', Coordinates),
		  ('Accessories', Accessories),
		  ('Slippers', Slippers),
		  ('Stockings', Stockings),
		  ('TopsBottomsNight', TopsBottomsNight),
		  ('HKMXBras', HKMXBras),
		  ('HKMXApparel', HKMXApparel),
		  ('HKMXSwimwear', HKMXSwimwear),
		  ('HKMXAccessories', HKMXAccessories)
  ) c (col, value)
  --WHERE plant = 2334
) src
pivot
(
  sum(value)
  for T in (
      [Bras Som]
    , [Bras Aantal]
    , [Bras-briefs Som]
    , [Bras-briefs Aantal]
    , [Bottoms Som]
    , [Bottoms Aantal]
    , [Top Som]
    , [Tops Aantal]
    , [Lingerie Som]
    , [Lingerie Aantal]
    , [Shapewear Som]
    , [Shapewear Aantal]
    , [Nightshirts Som]
    , [Nightshirts Aantal]
    , [Pyjamas Som]
    , [Pyjamas Aantal]
    , [Robes Som]
    , [Robes Aantal]
    , [Bathing Suits Som]
    , [Bathing Suits Aantal]
    , [Tops bottoms swim Som]
    , [Tops Bottoms Swim Aantal]
    , [Coordinatess Som]
    , [Coordinates Aantal]
    , [Accessories Som]
    , [Accessories Aantal]
    , [Slippers Som]
    , [Slippers Aantal]
    , [Stockings Som]
    , [Stockings Aantal]
    , [Tops Bottoms Night Som]
    , [Tops Bottoms Night Aantal]
    , [HKMX Bras Som]
    , [HKMX Bras Aantal]
    , [HKMX Apparel Som]
    , [HKMX Apparel Aantal]
    , [HKMX Swimwear Som]
    , [HKMX Swimwear Aantal]
    , [HKMX Accessories Som]
    , [HKMX Accesories Aantal]
	, [Total Som]
	, [Total Aantal]
)
) piv

