--select x.[Region ID], sum(customers)/sum([Pieces Sold]) [UPT]
--, sum(upt) [SumUPT], avg(upt) [AvgUPT]
--from 
--(
select [Country ID], [Region ID], [Store ID], [Fiscal Year], [Fiscal Week]
, sum([customers]) customers
, sum([pieces sold]) piecesSold
, convert(numeric(18,2)
	, iif(nullif(sum(customers),0) is null or nullif(sum([pieces sold]),0) is null
			, null
			, sum([customers]) / sum([pieces sold])
			)
	) as upt
from hana.dbo.[Tableau CV009 - CV010 - WEEKLY]
where [Fiscal Week] = 32 and [Fiscal Year]=2017
--and [Store ID] = 2806
group by rollup ([Fiscal Year], [Fiscal Week], [Country ID], [Region ID], [Store ID])
--order by [Fiscal Year], [Fiscal Week], [Country ID], [Region ID], [Store ID]
--) x
--group by x.[Region ID]