;WITH CTE AS (
SELECT
plant, z_recnum2, MATERIAL_MATL_GRP_2, XHR_QTYS
--20180629 Need the receipt total count
, (select sum(xhr_qtys) ReceiptTotal from ds_SAP_CV012_POS_SALESPLAN_Daily_PFY x where x.Z_RECNUM2 = c.Z_RECNUM2 
and  MATERIAL_MATL_GRP_2 in
(
  '22003', '22004', '22005', '22006', '22007'
, '22008', '22010', '22011', '22012', '22013'
, '22014', '22015', '22016', '22017', '22019'
, '22040', '22041', '22042', '22043', '22044'
)
 ) ReceiptTotal
FROM ds_SAP_CV012_POS_SALESPLAN_Daily_PFY c
WHERE 
--plant in ( 2712) --, 2339) and  
MATERIAL_MATL_GRP_2 in
(
  '22003', '22004', '22005', '22006', '22007'
, '22008', '22010', '22011', '22012', '22013'
, '22014', '22015', '22016', '22017', '22019'
, '22040', '22041', '22042', '22043', '22044'
)
and FISCPER <= 2017021
and PLANT_DISTR_CHAN = 'R1'
)

SELECT
*
INTO #tt1
FROM CTE
PIVOT (SUM(xhr_qtys) FOR MATERIAL_MATL_GRP_2 
IN (
  [22003], [22004], [22005], [22006], [22007]
, [22008], [22010], [22011], [22012], [22013]
, [22014], [22015], [22016], [22017], [22019]
, [22040], [22041], [22042], [22043], [22044]

)) AS PIVOTED 
order by 1

select *
into AJ_CrossSellPFY_v2
from 
(
select 'Bras Som' [Type], plant
, sum([22003]) Bras, sum([22004]) BrasBriefs, sum([22005]) Bottoms, sum([22006]) Tops, sum([22007]) Lingerie
, sum([22008]) Shapewear, sum([22010]) Nightshirts, sum([22011]) Pyjamas, sum([22012]) Robes, sum([22013]) BathingSuits
, sum([22014]) TopsBottomsSwim, sum([22015]) Coordinates , sum([22016]) Accessories, sum([22017]) Slippers, sum([22019]) Stockings
, sum([22040]) TopsBottomsNight, sum([22041]) HKMXBras, sum([22042]) HKMXApparel, sum([22043]) HKMXSwimwear, sum([22044]) HKMXAccessories
from #tt1 where nullif([22003],0) is not null
group by plant
union
select 'Bras Aantal', plant
,count([22003]), count([22003]),count([22003]),count([22003]),count([22003])
,count([22003]), count([22003]),count([22003]),count([22003]),count([22003])
,count([22003]), count([22003]),count([22003]),count([22003]),count([22003])
,count([22003]), count([22003]),count([22003]),count([22003]),count([22003])
from #tt1 where nullif([22003],0) is not null
group by plant
union
select 'Bras-briefs Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22004],0) is not null
group by plant
union
select 'Bras-briefs Aantal', plant
, count([22004]), count([22004]),count([22004]),count([22004]),count([22004])
, count([22004]), count([22004]),count([22004]),count([22004]),count([22004])
, count([22004]), count([22004]),count([22004]),count([22004]),count([22004])
, count([22004]), count([22004]),count([22004]),count([22004]),count([22004])
from #tt1 where nullif([22004],0) is not null
group by plant

union
select 'Bottoms Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22005],0) is not null
group by plant
union
select 'Bottoms Aantal', plant
, count([22005]), count([22005]),count([22005]),count([22005]),count([22005])
, count([22005]), count([22005]),count([22005]),count([22005]),count([22005])
, count([22005]), count([22005]),count([22005]),count([22005]),count([22005])
, count([22005]), count([22005]),count([22005]),count([22005]),count([22005])
from #tt1 where nullif([22005],0) is not null
group by plant
union
select 'Top Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22006],0) is not null
group by plant
union
select 'Tops Aantal', plant
, count([22006]), count([22006]),count([22006]),count([22006]),count([22006])
, count([22006]), count([22006]),count([22006]),count([22006]),count([22006])
, count([22006]), count([22006]),count([22006]),count([22006]),count([22006])
, count([22006]), count([22006]),count([22006]),count([22006]),count([22006])
from #tt1 where nullif([22006],0) is not null
group by plant
union
select 'Lingerie Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22007],0) is not null
group by plant
union
select 'Lingerie Aantal', plant
, count([22007]), count([22007]),count([22007]),count([22007]),count([22007])
, count([22007]), count([22007]),count([22007]),count([22007]),count([22007])
, count([22007]), count([22007]),count([22007]),count([22007]),count([22007])
, count([22007]), count([22007]),count([22007]),count([22007]),count([22007])
from #tt1 where nullif([22007],0) is not null
group by plant
union
select 'Shapewear Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22007],0) is not null
group by plant
union
select 'Shapewear Aantal', plant
, count([22008]), count([22008]),count([22008]),count([22008]),count([22008])
, count([22008]), count([22008]),count([22008]),count([22008]),count([22008])
, count([22008]), count([22008]),count([22008]),count([22008]),count([22008])
, count([22008]), count([22008]),count([22008]),count([22008]),count([22008])
from #tt1 where nullif([22008],0) is not null
group by plant

union
select 'Nightshirts Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22010],0) is not null
group by plant
union
select 'Nightshirts Aantal', plant
, count([22010]), count([22010]),count([22010]),count([22010]),count([22010])
, count([22010]), count([22010]),count([22010]),count([22010]),count([22010])
, count([22010]), count([22010]),count([22010]),count([22010]),count([22010])
, count([22010]), count([22010]),count([22010]),count([22010]),count([22010])
from #tt1 where nullif([22010],0) is not null
group by plant
union
select 'Pyjamas Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22011],0) is not null
group by plant
union
select 'Pyjamas Aantal', plant
, count([22011]), count([22011]),count([22011]),count([22011]),count([22011])
, count([22011]), count([22011]),count([22011]),count([22011]),count([22011])
, count([22011]), count([22011]),count([22011]),count([22011]),count([22011])
, count([22011]), count([22011]),count([22011]),count([22011]),count([22011])
from #tt1 where nullif([22011],0) is not null
group by plant
union
select 'Robes Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22012],0) is not null
group by plant
union
select 'Robes Aantal', plant
, count([22012]), count([22012]),count([22012]),count([22012]),count([22012])
, count([22012]), count([22012]),count([22012]),count([22012]),count([22012])
, count([22012]), count([22012]),count([22012]),count([22012]),count([22012])
, count([22012]), count([22012]),count([22012]),count([22012]),count([22012])
from #tt1 where nullif([22012],0) is not null
group by plant
union
select 'Bathing Suits Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22013],0) is not null
group by plant
union
select 'Bathing Suits Aantal', plant
, count([22013]), count([22013]),count([22013]),count([22013]),count([22013])
, count([22013]), count([22013]),count([22013]),count([22013]),count([22013])
, count([22013]), count([22013]),count([22013]),count([22013]),count([22013])
, count([22013]), count([22013]),count([22013]),count([22013]),count([22013])
from #tt1 where nullif([22013],0) is not null
group by plant
union
select 'Tops bottoms swim Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22014],0) is not null
group by plant
union
select 'Tops Bottoms Swim Aantal', plant
, count([22014]), count([22014]),count([22014]),count([22014]),count([22014])
, count([22014]), count([22014]),count([22014]),count([22014]),count([22014])
, count([22014]), count([22014]),count([22014]),count([22014]),count([22014])
, count([22014]), count([22014]),count([22014]),count([22014]),count([22014])
from #tt1 where  nullif([22014],0) is not null
group by plant
union
select 'Coordinatess Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22015],0) is not null
group by plant

union
select 'Coordinates Aantal', plant
, count([22015]), count([22015]),count([22015]),count([22015]),count([22015])
, count([22015]), count([22015]),count([22015]),count([22015]),count([22015])
, count([22015]), count([22015]),count([22015]),count([22015]),count([22015])
, count([22015]), count([22015]),count([22015]),count([22015]),count([22015])
from #tt1 where  nullif([22015],0) is not null
group by plant
union
select 'Accessories Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22016],0) is not null
group by plant
union
select 'Accessories Aantal', plant
, count([22016]), count([22016]),count([22016]),count([22016]),count([22016])
, count([22016]), count([22016]),count([22016]),count([22016]),count([22016])
, count([22016]), count([22016]),count([22016]),count([22016]),count([22016])
, count([22016]), count([22016]),count([22016]),count([22016]),count([22016])
from #tt1 where  nullif([22016],0) is not null
group by plant
union
select 'Slippers Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22017],0) is not null
group by plant
union
select 'Slippers Aantal', plant
, count([22017]), count([22017]),count([22017]),count([22017]),count([22017])
, count([22017]), count([22017]),count([22017]),count([22017]),count([22017])
, count([22017]), count([22017]),count([22017]),count([22017]),count([22017])
, count([22017]), count([22017]),count([22017]),count([22017]),count([22017])
from #tt1 where nullif([22017],0) is not null
group by plant
union
select 'Stockings Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22019],0) is not null
group by plant
union
select 'Stockings Aantal', plant
, count([22019]), count([22019]),count([22019]),count([22019]),count([22019])
, count([22019]), count([22019]),count([22019]),count([22019]),count([22019])
, count([22019]), count([22019]),count([22019]),count([22019]),count([22019])
, count([22019]), count([22019]),count([22019]),count([22019]),count([22019])
from #tt1 where nullif([22019],0) is not null
group by plant
union
select 'Tops Bottoms Night Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22040],0) is not null
group by plant
union
select 'Tops Bottoms Night Aantal', plant
, count([22040]), count([22040]),count([22040]),count([22040]),count([22040])
, count([22040]), count([22040]),count([22040]),count([22040]),count([22040])
, count([22040]), count([22040]),count([22040]),count([22040]),count([22040])
, count([22040]), count([22040]),count([22040]),count([22040]),count([22040])
from #tt1 where  nullif([22040],0) is not null
group by plant

union
select 'HKMX Bras Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22041],0) is not null
group by plant
union
select 'HKMX Bras Aantal', plant
, count([22041]), count([22041]),count([22041]),count([22041]),count([22041])
, count([22041]), count([22041]),count([22041]),count([22041]),count([22041])
, count([22041]), count([22041]),count([22041]),count([22041]),count([22041])
, count([22041]), count([22041]),count([22041]),count([22041]),count([22041])
from #tt1 where  nullif([22041],0) is not null
group by plant
union
select 'HKMX Apparel Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22042],0) is not null
group by plant
union
select 'HKMX Apparel Aantal', plant
, count([22042]), count([22042]),count([22042]),count([22042]),count([22042])
, count([22042]), count([22042]),count([22042]),count([22042]),count([22042])
, count([22042]), count([22042]),count([22042]),count([22042]),count([22042])
, count([22042]), count([22042]),count([22042]),count([22042]),count([22042])
from #tt1 where  nullif([22042],0) is not null
group by plant
union
select 'HKMX Swimwear Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22043],0) is not null
group by plant
union
select 'HKMX Swimwear Aantal', plant
, count([22043]), count([22043]),count([22043]),count([22043]),count([22043])
, count([22043]), count([22043]),count([22043]),count([22043]),count([22043])
, count([22043]), count([22043]),count([22043]),count([22043]),count([22043])
, count([22043]), count([22043]),count([22043]),count([22043]),count([22043])
from #tt1 where  nullif([22043],0) is not null
group by plant

union
select 'HKMX Accessories Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([22044],0) is not null
group by plant

union
select 'HKMX Accesories Aantal', plant
, count([22044]), count([22044]),count([22044]),count([22044]),count([22044])
, count([22044]), count([22044]),count([22044]),count([22044]),count([22044])
, count([22044]), count([22044]),count([22044]),count([22044]),count([22044])
, count([22044]), count([22044]),count([22044]),count([22044]),count([22044])
from #tt1 where  nullif([22044],0) is not null
group by plant

union
select 'Total Som', plant
, sum([22003]), sum([22004]), sum([22005]), sum([22006]), sum([22007])
, sum([22008]), sum([22010]), sum([22011]), sum([22012]), sum([22013])
, sum([22014]), sum([22015]), sum([22016]), sum([22017]), sum([22019])
, sum([22040]), sum([22041]), sum([22042]), sum([22043]), sum([22044])
from #tt1 where nullif([ReceiptTotal],0) is not null
group by plant

union
select 'Total Aantal', plant
, count([ReceiptTotal]), count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal])
, count([ReceiptTotal]), count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal])
, count([ReceiptTotal]), count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal])
, count([ReceiptTotal]), count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal]),count([ReceiptTotal])
from #tt1 where nullif([ReceiptTotal],0) is not null
group by plant

) x

drop table #tt1