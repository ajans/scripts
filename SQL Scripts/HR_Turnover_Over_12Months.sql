declare @startPoint date = '2016-02-01'
declare @endPoint date = '2018-02-28'
--hq 30% level
select distinct s.calendarDate, s.locationId
,  openCount.headcount, outFlowForLast12Months.leaverCount
from stage_010_HR_Workers_Active_in_Period s
outer apply
(
	select count(distinct employeeID) headCount
	from dbo.ds_workday_HR_Workers_Active_in_Period x
	where x.terminationDate is null
	and x.hiredate <= s.calendarDate
	AND x.locationID = s.locationID
	AND x.costCenterId  = s.costcenterid
	and x.employeeId not in 
	(
		select DISTINCT employeeID
		from dbo.ds_workday_HR_Workers_Active_in_Period
		where contractType not in
		('Fixed term (fixed hours)', 'Fixed term (min-max hours)', 'Intern'
		, 'School Trainee', 'Unpaid Trainee', 'Work student')
		AND ((month(hireDate) >= 6 and month(hireDate) <= 8) or month(hiredate) = 12)
		AND ((month(terminationDate) >= 6 and month(terminationDate) <= 8) or month(terminationDate) = 12)
		AND locationID like '[0-9][0-9][0-9][0-9]'
		AND datediff(mm, hireDate, terminationdate) <= 3
	)
) openCount
OUTER apply
(
    	select count(distinct employeeID) leaverCount
	from dbo.ds_workday_HR_Workers_Active_in_Period z
	WHERE z.terminationDate BETWEEN  dateadd(month, -12, s.calendarDate) AND dateadd(dd,-1, s.calendarDate)
	AND z.locationID = s.locationid  
	AND z.costCenterId  = s.costcenterid
	and z.employeeId not in 
	(
		select DISTINCT employeeID
		from dbo.ds_workday_HR_Workers_Active_in_Period
		where contractType not in
		('Fixed term (fixed hours)', 'Fixed term (min-max hours)', 'Intern'
		, 'School Trainee', 'Unpaid Trainee', 'Work student')
		AND ((month(hireDate) >= 6 and month(hireDate) <= 8) or month(hiredate) = 12)
		AND ((month(terminationDate) >= 6 and month(terminationDate) <= 8) or month(terminationDate) = 12)
		AND locationID like '[0-9][0-9][0-9][0-9]'
		AND datediff(mm, hireDate, terminationdate) <= 3
	)
) outFlowForLast12Months

where (s.calendarDate between @startPoint  and @endPoint
and day(s.calendarDate) = 1 or calendarDate = @endPoint)
--AND s.locationID = 'HQ_NL'
AND s.locationCountry = 'Netherlands'
AND isnumeric(s.locationID) = 1
--AND s.costCenterId	= '61800'
order by s.calendarDate

--select DISTINCT employeeID
--from stage_010_HR_Workers_Active_in_Period
--where contractType not in
--('Fixed term (fixed hours)', 'Fixed term (min-max hours)', 'Intern'
--, 'School Trainee', 'Unpaid Trainee', 'Work student')
--or ((month(hireDate) >= 6 and month(hireDate) <= 8) or month(hiredate) = 12)
--or ((month(terminationDate) >= 6 and month(terminationDate) <= 8) or month(terminationDate) = 12)
--or locationID like '[0-9][0-9][0-9][0-9]'
--or datediff(mm, hireDate, terminationdate) <= 3

/*
--01/02/2017 - 31/01/2018
--Jan
--open headcount at start of 2017
select count(distinct employeeId) 
from ds_workday_HR_Workers_Active_in_Period wrk
where hireDate <= '2017-01-01'
and terminationDate is null
and (
--exclude seasonal workers sales org
--Fixed TErm Contract types
contractType not in (fixed term fixed hours, fixed term min max, intern, school trainee, unpaid trainee, work student)
or
--hire date in season
hireDate = (month >= 6 and <= 8) or (month = 12)
or
--termindation date in season
termination date = (month >= 6 or month<= 9) or month = 12
--location store or not
iif location is 4 digits then store
-- date betwen hire / fire 
datediff hire termination <= 3
)
--total leavers during 2017
select count(distinct employeeId) 
from ds_workday_HR_Workers_Active_in_Period wrk
where terminationDate >= '2017-01-01'
and terminationDate < '2018-01-01'
and (
--exclude seasonal workers sales org
--Fixed TErm Contract types
contractType not in (fixed term fixed hours, fixed term min max, intern, school trainee, unpaid trainee, work student)
or
--hire date in season
hireDate = (month >= 6 and <= 8) or (month = 12)
or
--termindation date in season
termination date = (month >= 6 or month<= 9) or month = 12
--location store or not
iif location is 4 digits then store
-- date betwen hire / fire 
datediff hire termination <= 3
)
*/


--1/2/17 - 31/1/18
SELECT 
  locationCountry
, jobProfile
, count(*)
FROM dbo.ds_workday_HR_Workers_Active_in_Period ds_workday_HR_Workers_Active_in_Period
WHERE ds_workday_HR_Workers_Active_in_Period.jobProfile	
IN (
'Lingerie Ambassador' 
, 'Fashionista'
, 'Lingerie Expert'
, 'Lingerista', 'Lingersta'
)
--AND locationCountry = 'Austria'
AND ds_workday_HR_Workers_Active_in_Period.hireDate <= '2017-02-01'
AND (ds_workday_HR_Workers_Active_in_Period.terminationDate  IS NULL OR ds_workday_HR_Workers_Active_in_Period.terminationDate > '2017-02-01')
AND locationCountry IS NOT null
GROUP BY locationCountry, ds_workday_HR_Workers_Active_in_Period.jobProfile
ORDER BY locationCountry, ds_workday_HR_Workers_Active_in_Period.jobProfile

SELECT 
locationCountry
, jobProfile
, count(*)
FROM dbo.ds_workday_HR_Workers_Active_in_Period ds_workday_HR_Workers_Active_in_Period
WHERE ds_workday_HR_Workers_Active_in_Period.jobprofile	
IN (
'Lingerie Ambassador' 
, 'Fashionista'
, 'Lingerie Expert'
, 'Lingerista', 'Lingersta'
)
--AND locationCountry = 'Austria'
AND ds_workday_HR_Workers_Active_in_Period.terminationDate  between '2017-02-01' AND '2018-01-31'
AND terminationDate > hireDate
AND locationCountry IS NOT null
GROUP BY locationCountry, ds_workday_HR_Workers_Active_in_Period.jobProfile
ORDER BY locationCountry, ds_workday_HR_Workers_Active_in_Period.jobProfile

/*
Cal month 		StartDate	EndDate
1	1-1-2018	1-1-2017	31-12-2017
2	1-2-2018	1-2-2017	31-1-2018
3	1-3-2018	1-3-2017	28-2-2018
4	1-4-2018	1-4-2017	31-3-2018
5	1-5-2018	1-5-2017	30-4-2018
6	1-6-2018	1-6-2017	31-5-2018
7	1-7-2018	1-7-2017	30-6-2018
8	1-8-2018	1-8-2017	31-7-2018
9	1-9-2018	1-9-2017	31-8-2018
10	1-10-2018	1-10-2017	30-9-2018
11	1-11-2018	1-11-2017	31-10-2018
12	1-12-2018	1-12-2017	30-11-2018

*/



SELECT d.calendarDate, count(distinct d.employeeID) [startingCount], max(outs.Outflow) OutFlow, dateadd(dd, -1, d.calendarDate) , dateadd(year,-1,d.calendarDate)
, LAG(count(DISTINCT d.employeeID), 12,0) OVER (ORDER BY d.calendarDate) AS PreviousCount
, max(outs.Outflow) / convert(float,nullif(LAG(count(DISTINCT d.employeeID), 12,0) OVER (ORDER BY d.calendarDate),0.00))
FROM dbo.view_Tableau_HR_Headcount_Data d
OUTER APPLY
(
    SELECT Outflow = count(DISTINCT v.employeeid)
    FROM dbo.view_Tableau_HR_Headcount_Data v
    WHERE v.TerminationDate BETWEEN dateadd(year,-1,d.calendarDate) AND dateadd(dd, -1, d.calendarDate)
    AND v.Outflow	 = 1
    AND v.LocationCountry   = 'Austria'
    AND v.JobProfile    IN (
					   'Lingerie Ambassador' 
					   , 'Fashionista'
					   , 'Lingerie Expert'
					   , 'Lingerista', 'Lingersta'
					   )

) outs
WHERE day(d.calendarDate) = 1
AND year(d.calendarDate) IN (2016, 2017, 2018)
AND d.Outflow  != 1
AND d.jobprofile	
IN (
'Lingerie Ambassador' 
, 'Fashionista'
, 'Lingerie Expert'
, 'Lingerista', 'Lingersta'
)
AND d.locationCountry = 'Austria'
GROUP BY d.calendarDate
ORDER BY year(d.calendarDate), month(d.calendarDate)
