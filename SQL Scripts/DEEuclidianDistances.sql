--with
--  distances as (
--    select
--      sqrt(power(d.lat - s.lat, 2) 
--          + power(d.long - s.long, 2)) as distance
--      , s.team_name as src
--      , d.team_name as dest
--    from
--      mlb_stadiums s
--      , mlb_stadiums d
--    where
--      s.team_name != d.team_name
--  )

declare @table table
(
	source nvarchar(1000)
	, destination nvarchar(1000)
	, distance float
);

with
distances as
(
  select s.storeName as src
  , d.storeName as dest
  , sqrt(power(convert(float, replace(d.storeLatitude,',', '.')) - convert(float, replace(s.storeLatitude,',', '.')) , 2)
		 + power(convert(float, replace(d.storeLongitude, ',', '.')) - convert(float, replace(s.storeLongitude, ',', '.')),2)) dist
  from stage_010_ECC_Master_Store_inc_Change_full s, stage_010_ECC_Master_Store_inc_Change_full d
  where s.countryID = 'DE'
  and s.channelID = 'R1'
  and d.countryID = 'DE'
  and d.channelID = 'R1'
  and s.storeLatitude is not null
  and d.storeLatitude is not null
  and s.storeName != d.storeName
  )
  insert into @table
  select * from distances 
  
  select t.*, s.storeid, s.storeOpeningDate, d.storeid, d.storeOpeningDate
  from @table t 
  inner join stage_010_ECC_Master_Store_inc_Change_full s
  on s.storeName = t.source
  inner join stage_010_ECC_Master_Store_inc_Change_full d
  on d.storeName = t.destination
  where distance <0.1

  and s.storeOpeningDate < d.storeOpeningDate
  and year(d.storeOpeningDate) >= 2016
  and year(s.storeOpeningDate) <= 2016
  and s.storeClosingDate is null
  order by t.source, t.distance

  