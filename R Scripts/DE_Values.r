install.packages("rio")
install.packages("dplyr")
install.packages("ggplot2")  
install.packages("Hmisc")
library(dplyr)
library(readr)

library(readr)
DE_Values <- read_delim("C:/Temp/DE_Values.csv", 
                        ";", escape_double = FALSE, col_types = cols(ATV = col_number(), 
                                                                     Customers = col_number(), CustomersCorrected = col_number(), 
                                                                     FiscalYearPeriod = col_integer(), 
                                                                     GrossSales = col_number(), GrossSalesExPromo = col_number(), 
                                                                     PiecesSold = col_number(), PiecesSoldExPromo = col_number(), 
                                                                     UPT = col_number(), Visitors = col_number(), 
                                                                     VisitorsCorrected = col_number(), 
                                                                     WeekEnd = col_date(format = "%Y-%m-%d"), 
                                                                     WeekStart = col_date(format = "%Y-%m-%d"), 
                                                                     bhp = col_integer(), fiscalPeriod = col_integer(), 
                                                                     fittingRooms = col_integer(), regionLatitude = col_number(), 
                                                                     regionLongitude = col_number(), sellingAreaM2 = col_integer(), 
                                                                     storeLatitude = col_number(), storeLongitude = col_number()), 
                        na = "empty", trim_ws = TRUE)
View(DE_Values)


oldest <- min(DE_Values$WeekStart, na.rm = TRUE)

DE_Values %>% 
  mutate(delays = difftime(DE_Values$WeekStart, oldest, units = "days")) -> DE_Values_mutation

DE_Values_mutation = subset(DE_Values_mutation, storeConceptName!= 'Shop-in-Shop')

DE_Values_mutation %>%
  select(UPT) %>%
  unlist() %>%
  fivenum() -> quartiles

DE_Values_mutation %>%
  select(UPT) %>%
  unlist() %>%
  IQR(na.rm = TRUE)

DE_Values_mutation %>%
  select(UPT) %>%
  unlist() %>%
  sd(na.rm = TRUE)

q <- quartiles[2:4]
skewness <- ((q[3]- q[2])-(q[2]- q[1]))/(q[3]-q[1])

cor(x = as.numeric(DE_Values_mutation$delays),
    y = DE_Values_mutation$ATV, use="complete.obs") 

install.packages("energy")

#energy::dcor(DE_Values_mutation$delays, DE_Values_mutation$ATV)
library(ggplot2)
ggplot(data = DE_Values_mutation, aes(DE_Values_mutation$ATV)) + geom_histogram(bins = 3)

boxplot(x = DE_Values_mutation$ATV, horizontal = TRUE)
boxplot(x = DE_Values_mutation$UPT, horizontal = TRUE)
boxplot(x = DE_Values_mutation$GrossSalesExPromo, horizontal = TRUE)
boxplot.stats(x = DE_Values_mutation$ATV)

colnames(DE_Values_mutation)
 lm(storeID ~ sellingAreaM2, DE_Values_mutation) 
 DE_Values_mutation <- na.omit(DE_Values_mutation)
 
 fit <- kmeans(DE_Values_mutation[,c("storeLatitude", "storeLongitude")],3)
 fit
 
 plot(DE_Values_mutation$storeLatitude, DE_Values_mutation$storeLongitude, col = fit$cluster)
 
 fit <- kmeans(DE_Values_mutation[,c("UPT", "sellingAreaM2")],5)
 plot(DE_Values_mutation$UPT, DE_Values_mutation$sellingAreaM2, col = fit$cluster)
 
unique(DE_Values_mutation$storeConceptName)
cityCenter <- DE_Values_mutation[ which(DE_Values_mutation$storeConceptName=='City Center'),]
shoppingCenter <- DE_Values_mutation[ which(DE_Values_mutation$storeConceptName=='Shopping Center'),]
outletCenter <- DE_Values_mutation[ which(DE_Values_mutation$storeConceptName=='Outlet Center'),]

piecesSold <- c(cityCenter$PiecesSoldExPromo, shoppingCenter$PiecesSoldExPromo, outletCenter$PiecesSoldExPromo)
UPT <- c(cityCenter$UPT, shoppingCenter$UPT, outletCenter$UPT)
ATV <- c(cityCenter$ATV, shoppingCenter$ATV, outletCenter$ATV)
storeType <- c(rep("City Center",5744), rep("Outlet Center",312), rep("Shopping Center",5547))

boxplot(piecesSold ~ storeType,xlab='Store Type', ylab = "Pieces Sold", main="Pieces Sold boxplots of different Store Types")
boxplot(ATV ~ storeType,xlab='Store Type', ylab = "ATV", main="ATV boxplots of different Store Types")
boxplot(UPT ~ storeType,xlab='Store Type', ylab = "UPT", main="UPT boxplots of different Store Types")

oneway.test(piecesSold ~ storeType)

pieces.aov<- aov(piecesSold ~ storeType)
summary(pieces.aov)
model.tables(pieces.aov, "means")

pieces.posthoc<-TukeyHSD(pieces.aov)
pieces.posthoc