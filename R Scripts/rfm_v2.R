#install.packages('DataExplorer')
library(dplyr)
library(ggplot2)
library(readr)
library(DataExplorer)
# Load data
sales_data <- daily_data #read.csv("data/s4.csv",stringsAsFactors=FALSE)
# Review data
str(sales_data)
View(sales_data)
summary(sales_data)
# Create recency
sales_data$Recency <- round(as.numeric(difftime(Sys.Date(),
                                                sales_data[,2],units="days")) )
# Creation of Recency, Frequency y Monetary
salesM <-
  aggregate(sales_data[,5],list(sales_data$ID),sum)
names(salesM) <- c("idCustomer","Monetary")
salesF <-
  aggregate(sales_data[,5],list(sales_data$ID),length)
names(salesF) <- c("idCustomer","Frequency")
salesR <-
  aggregate(sales_data[,7],list(sales_data$ID),min)
names(salesR) <- c("idCustomer","Recency")
# Combination R,F,M
temp <- merge(salesF,salesR,"idCustomer")
salesRFM <- merge(temp,salesM,"idCustomer")
# Creation of R,F,M rank
salesRFM$rankR <- cut(salesRFM$Recency,5,labels=F)
salesRFM$rankF <- cut(salesRFM$Frequency,5,labels=F)
salesRFM$rankM <- cut(salesRFM$Monetary,5,labels=F)
# Review top 10
salesRFM <- salesRFM[with(salesRFM, order(-Recency, -Frequency, -Monetary)), ]

head(salesRFM, n=10)
# Analysis
groupRFM <- count(salesRFM, Recency, Frequency, Monetary)
groupRFM <- salesRFM$Recency*100 + salesRFM$Frequency*10 +  salesRFM$Monetary
salesRFM <- cbind(salesRFM,groupRFM)
# Plot
ggplot(salesRFM, aes(factor(groupRFM))) +
  geom_bar() +
  ggtitle('Customer Distribution by RFM') +
  labs(x="RFM",y="# Customers") +
  theme(plot.title = element_text(color="#666666", face="bold",
                                  size=16, hjust=0)) +
  theme(axis.title = element_text(color="#666666", face="bold"))