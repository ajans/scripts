
# coding: utf-8

# In[ ]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')


# In[ ]:


sns.set()


# In[ ]:


#export of member cards where the age range is 16 to 100 inclusive and who have purchased something in 2017 or 2018
members = pd.read_csv('Data/Data.csv', encoding='utf-8', parse_dates=True, engine='python', error_bad_lines=False)
members.head()


# In[ ]:


#export of store info
stores = pd.read_csv('Data/Stores.csv',  parse_dates=True, engine='python', error_bad_lines=False)
# only keep NL
stores = stores[(stores['countryID'] == 'NL')]
stores.head()


# In[ ]:


#read the export of sales for NL 2018 using dask
import dask.dataframe as dd

dtypes = {'plant': np.int64, 'PlantCity': str, 'fiscper': str, 'Receipt': str, 'GenericID': str
           , 'ArticleVariant': str, 'ProductGroup': str, 'MaterialGroup': str, 'MemberID': np.int64, 'GrossSales': np.float32
         }

salesDask = dd.read_csv('Data/Sales.csv', dtype=dtypes, assume_missing=True)
   
salesDask.head()


# In[ ]:


#create a pandas dataframe
sales = salesDask.compute()


# In[ ]:


# only intersted in NL members for now
# 2,021,643 NL members in the full data dump 1,107,147 remain who have purchased in last 24 months (~55%)
members = members[(members['COUNTRY'] == 'NL')]


# In[ ]:


plt.hist(members['Age'], bins=7, density=True, alpha=0.5,
         histtype='stepfilled', color='steelblue',
         edgecolor='none');


# In[ ]:


members.groupby('COUNTRY')['Age'].describe()


# In[ ]:


#join the data frames together
salesMembers = pd.merge(sales, members, left_on='MemberID', right_on='memberid').drop('memberid', axis=1)


# In[ ]:


## bin the member ages 
bins = pd.IntervalIndex.from_tuples([(15, 20), (21, 26), (27, 31), (32, 37), (38, 43), (43, 54), (55, 100)])
salesMembers['ageBin'] = pd.cut(salesMembers['Age'],bins, labels=range(7))
#salesMembers['ageBin'].unique()


# In[ ]:


salesMembers.dtypes


# In[ ]:


#enrich sales + members with store attributes and join the data frames together
salesMembers = pd.merge(salesMembers, stores, left_on='plant', right_on='storeID').drop('plant', axis=1)
salesMembers.head()


# In[ ]:


#2349 revenue should be around EUR 2,828,896
dfChannel = pd.DataFrame(salesMembers)
#dfChannel.drop(dfChannel[dfChannel.Age > 51].index, inplace=True)
#dfChannel.groupby(['channelName', 'Age']).GrossSales.sum()
dfChannel.groupby(['channelName', 'storeID']).GrossSales.sum()


# In[ ]:


##drop na
#salesMembers.dropna(inplace=True)


# In[ ]:


##check for nulls
#salesMembers.isnull().any()


# In[ ]:


#store sales per age bucket sum the gross sales
storeSales = pd.DataFrame(salesMembers.groupby(['storeID', 'ageBin']).GrossSales.sum())
storeSales.head()


# In[ ]:


#sum per age the gross sales
ageSales = pd.DataFrame(salesMembers.groupby(['Age'], as_index=False).GrossSales.sum())
ageSales.head()


# In[ ]:


#ageSales.dtypes
sns.set(rc={'figure.figsize':(20,8.27)})
sns.barplot(x=ageSales['Age'], y=ageSales['GrossSales'])


# In[ ]:


data = pd.DataFrame(salesMembers.groupby(['Age', 'channelName'], as_index=False).GrossSales.sum())
sns.set(rc={'figure.figsize':(20,15)})
ax = sns.barplot(x="Age", y="GrossSales", hue="channelName", data=data)


# In[ ]:


dataC = pd.DataFrame(salesMembers.groupby(['Age', 'channelName'], as_index=False).GrossSales.sum())
dataC.head()


# In[ ]:


#sns.catplot(x="Age", y="GrossSales",col="channelName",data=data, kind="bar");

g = sns.FacetGrid(dataC, col="channelName", sharey=False, height=5, aspect=1)
g = g.map(plt.bar, "Age", "GrossSales")


# In[ ]:


dataP = pd.DataFrame(salesMembers.groupby(['Age', 'ProductGroup'], as_index=False).GrossSales.sum())
dataP.head()


# In[ ]:


g = sns.FacetGrid(dataP, col="ProductGroup", sharey=False, height=8, aspect=.9)
g = g.map(plt.bar, "Age", "GrossSales")


# In[ ]:


dataM = pd.DataFrame(salesMembers.groupby(['Age', 'MaterialGroup'], as_index=False).GrossSales.sum())
#dataM.head()
g = sns.FacetGrid(dataM, col="MaterialGroup", sharey=False, height=8, aspect=.9)
g = g.map(plt.bar, "Age", "GrossSales")


# In[ ]:


dataM = pd.DataFrame(salesMembers.groupby(['Age', 'storeID'], as_index=False).GrossSales.sum())
#dataM.head()
g = sns.FacetGrid(dataM, col="storeID", sharey=False, height=8, aspect=.9, col_wrap=6)
g = g.map(plt.bar, "Age", "GrossSales")


# In[ ]:


plt.hist(ageSales['GrossSales'], density=True, alpha=0.5,
         histtype='stepfilled', color='steelblue',
         edgecolor='none');


# In[ ]:


newdf = ageSales[['Age','GrossSales']].groupby('Age').sum()
#print(newdf)
newdf.plot(kind='bar', figsize=(15,5))


# In[ ]:


plt.plot(ageSales['Age'], ageSales['GrossSales'])


# In[ ]:


#sum per age and store the gross sales
ageSales = pd.DataFrame(salesMembers.groupby(['Age', 'storeID'], as_index=False).GrossSales.sum())
ageSales.head()


# In[ ]:


salesMembers.groupby('regionID')['Age'].mean()


# In[ ]:


dfRegionAge = pd.DataFrame(salesMembers[['regionID', 'Age']])
##taking the age restriction out for now #dfRegionAge = dfRegionAge[(dfRegionAge['Age'] <= 50)]
dfRegionAge.groupby(['regionID'], as_index=False).Age.mean()


# In[ ]:


#sum per age and store the gross sales
ageSales = pd.DataFrame(salesMembers.groupby(['Age', 'regionID'], as_index=False).GrossSales.sum())
ageSales.head()


# In[ ]:


##restrict the set to 50 and below
#ageSales = ageSales[(ageSales['Age'] <= 50)]
#ageSales.head()


# In[ ]:


##dubious on this, check thinking
ageSales['gsBIN'] = pd.cut(ageSales['GrossSales'],5, labels=range(5))
ageSales.head()


# In[ ]:


newdf2 = ageSales[['Age','regionID','gsBIN']]
#print(newdf2)
grid = sns.FacetGrid(newdf2, row="regionID", col="gsBIN", margin_titles=True)
grid.map(plt.hist, "Age");


# In[ ]:


sns.pairplot(ageSales, height=6, hue="regionID")


# In[ ]:


sns.set(style="ticks")

# Initialize the figure with a logarithmic x axis
f, ax = plt.subplots(figsize=(17, 16))
#ax.set_xscale("log")

# Plot the gross sales with horizontal boxes
sns.boxplot(x="GrossSales", y="regionID", data=ageSales,
            whis="range", palette="vlag")

# Add in points to show each observation
sns.swarmplot(x="GrossSales", y="regionID", data=ageSales,
              size=2, color=".3", linewidth=0)

# Tweak the visual presentation
ax.xaxis.grid(True)
ax.set(ylabel="")
sns.despine(trim=True, left=True)


# In[ ]:


#salesMembers['GrossSales'].dtype
#pd.to_numeric(salesMembers['GrossSales'])


# In[ ]:


#sum per age bucket the gross sales
sf = pd.DataFrame(salesMembers.groupby(['regionID', 'ageBin'], as_index=False, group_keys=False).GrossSales.agg(['sum', 'count']))
sf.head()


# In[ ]:


sf.dtypes
sf['grossSales'] = sf['sum']
#sf2 = sf.reset_index()
sf2 = sf.drop(columns=['sum', 'count'])
sf2.head()


# In[ ]:


#sf2=
sf2.unstack()
#sf2.head()


# In[ ]:


sf2.unstack().plot(kind='bar', subplots=True, layout=(4,2), figsize=(25,50))


# In[ ]:


productSales = pd.DataFrame(salesMembers.groupby(['Age', 'ProductGroup'], as_index=False).GrossSales.sum())
productSales.head()


# In[ ]:


sns.pairplot(productSales, height=6, hue="ProductGroup")


# In[ ]:


#product = pd.DataFrame(productSales[(productSales['Age'] <= 50)])
product = pd.DataFrame(productSales)
product.GrossSales = product.GrossSales.round()

product = product.pivot("Age", "ProductGroup", "GrossSales")
#product.describe

f, ax = plt.subplots(figsize=(20, 17))
sns.heatmap(product, annot=True, fmt='.1f', linewidths=.5, ax=ax)


# In[ ]:


productSales = pd.DataFrame(salesMembers.groupby(['Age', 'MaterialGroup'], as_index=False).GrossSales.sum())
productSales.head()


# In[ ]:


#product = pd.DataFrame(productSales[(productSales['Age'] <= 50)])
product = pd.DataFrame(productSales)
product['AgeBin'] = pd.cut(product['Age'],bins, labels=range(7))
product.GrossSales = product.GrossSales.round()

product = product.pivot("Age", "MaterialGroup", "GrossSales")
##product.describe

f, ax = plt.subplots(figsize=(30, 27))
sns.heatmap(product, annot=True, fmt='.1f', linewidths=.5, ax=ax)


# In[ ]:


import datetime
membersDates = pd.DataFrame(members)
#membersDates = pd.DataFrame(membersDates)
#membersDates.drop(membersDates[membersDates.Age > 51].index, inplace=True)
membersDates['ageBin'] = pd.cut(membersDates['Age'],bins, labels=range(7))
membersDates['DateBecomingMember_Norm'] = pd.to_datetime(membersDates['DATEBECOMINGMEMBER'], errors='coerce')

membersDates['year'] = membersDates['DateBecomingMember_Norm'].dt.year
membersDates['month'] = membersDates['DateBecomingMember_Norm'].dt.month

#membersDates.head()
#membersDates.Age.max()

#members.groupby(['year', 'month', 'Age']).count()
data = membersDates.groupby(['year', 'ageBin']).Age.count()


# In[ ]:


data = pd.DataFrame(membersDates.groupby(['year', 'ageBin'], as_index=False).Age.count())
data.rename(index=str, columns={"year": "year", "ageBin": "ageBin", "Age":"members"}, inplace=True)
#data.head()
sns.lineplot(x="year", y="members", hue="ageBin", data=data)


# In[ ]:


salesMembers['channelName'].unique()


# In[ ]:


temp = sales.groupby(['plant']).GrossSales.sum()


# In[ ]:


temp2 = pd.DataFrame(temp.head(20))
pd.options.display.float_format = '€{:,.2f}'.format
#temp2.dtypes
print(temp2)


# In[ ]:


salesMembers.head()


# In[ ]:


pd.options.display.float_format = '{:,.2f}'.format
df=salesMembers.groupby(['ageBin', 'GenericID']).Pieces.sum()


# In[ ]:


df.head(20)


# In[ ]:


dataC = pd.DataFrame(salesMembers.groupby(['channelName', 'fiscper'], as_index=False).GrossSales.sum())
dataC.head()


# In[ ]:


dataC.groupby(['channelName']).describe()


# In[ ]:


ownStores = dataC.loc[dataC['channelName'] == 'Own stores']
ecom =  dataC.loc[dataC['channelName'] == 'E-commerce']
sns.distplot(ownStores['GrossSales']);


# In[ ]:


sns.distplot(ecom['GrossSales']);


# In[ ]:


from scipy.stats import ttest_ind

# summarize
print('data1: mean=%.3f stdv=%.3f' % (np.mean(ownStores['GrossSales']), np.std(ownStores['GrossSales'])))
print('data2: mean=%.3f stdv=%.3f' % (np.mean(ecom['GrossSales']), np.std(ecom['GrossSales'])))

# compare samples
stat, p = ttest_ind(ownStores['GrossSales'], ecom['GrossSales'])
print('Statistics=%.3f, p=%.3f' % (stat, p))
# interpret
alpha = 0.05
if p > alpha:
	print('Same distributions (fail to reject H0)')
else:
	print('Different distributions (reject H0)')


# In[ ]:


dataD = pd.DataFrame(salesMembers.groupby(['ageBin', 'fiscper'], as_index=False).GrossSales.sum())
dataD.head()


# In[ ]:


dataD.groupby(['ageBin']).describe()


# In[ ]:


#a = 
dataD.head()
#dataD[dataD.ageBin == '(55, 100]']
#dataD.loc[dataD['ageBin'] > 21]
#b = dataD.loc[dataD['ageBin'] == '(27, 31]']
#a.head()


# In[ ]:


#from scipy.stats import ttest_ind

## summarize
#print('data1: mean=%.3f stdv=%.3f' % (np.mean(a['GrossSales']), np.std(a['GrossSales'])))
#print('data2: mean=%.3f stdv=%.3f' % (np.mean(b['GrossSales']), np.std(b['GrossSales'])))

## compare samples
#stat, p = ttest_ind(a['GrossSales'], b['GrossSales'])
#print('Statistics=%.3f, p=%.3f' % (stat, p))
## interpret
#alpha = 0.05
#if p > alpha:
#	print('Same distributions (fail to reject H0)')
#else:
#	print('Different distributions (reject H0)')


# In[ ]:


# Analysis of Variance test
from scipy.stats import f_oneway

# three  samples
data1 = dataC.loc[dataC['channelName'] == 'Own stores']
data2 = dataC.loc[dataC['channelName'] == 'E-commerce']
data3 = dataC.loc[dataC['channelName'] == 'Franchise Nat. ']


# In[ ]:


# compare samples
stat, p = f_oneway(data1['GrossSales'], data2['GrossSales'], data3['GrossSales'])
print('Statistics=%.3f, p=%.3f' % (stat, p))
# interpret
alpha = 0.05
if p > alpha:
	print('Same distributions (fail to reject H0)')
else:
	print('Different distributions (reject H0)')


# In[ ]:


dataD.groupby('ageBin').describe()


# In[ ]:


gen = salesMembers.groupby(['ageBin','GenericID'], as_index=False).Pieces.sum()
##gen.dtypes
gen = gen.sort_values('Pieces',ascending = False).groupby('ageBin').head(10)


# In[ ]:


gen.sort_values(['ageBin', 'Pieces'],ascending = False)

